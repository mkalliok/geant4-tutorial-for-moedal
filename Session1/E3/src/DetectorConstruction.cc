//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file DetectorConstruction.cc
/// \brief Adaptation of the B1DetectorConstruction class

#include "DetectorConstruction.hh"

#include "G4RunManager.hh"
#include "G4NistManager.hh"

#include "G4Box.hh"

#include "G4UnionSolid.hh"
#include "G4SubtractionSolid.hh"
#include "G4IntersectionSolid.hh"
#include "G4MultiUnion.hh"

#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4SystemOfUnits.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::DetectorConstruction()
: G4VUserDetectorConstruction(),
  fScoringVolume(0)
{ }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::~DetectorConstruction()
{ }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume* DetectorConstruction::Construct()
{  
  // Get nist material manager
  G4NistManager* nist = G4NistManager::Instance();
  
  // Option to switch on/off checking of volumes overlaps
  //
  G4bool checkOverlaps = true;

  //     
  // World
  //
  G4double world_sizeXY = 1.0*m;
  G4double world_sizeZ  = 1.0*m;
  G4Material* world_mat = nist->FindOrBuildMaterial("G4_AIR");
  
  auto solidWorld = new G4Box("World", 0.5*world_sizeXY, 0.5*world_sizeXY, 0.5*world_sizeZ);
  auto logicWorld = new G4LogicalVolume(solidWorld, world_mat, "World");
  auto physWorld = new G4PVPlacement(0, G4ThreeVector(), logicWorld, "World", 0, false, 0, checkOverlaps);
                     
  //     
  // Box1
  //
  G4double box1dim = 10.*cm;
  auto solidBox1 = new G4Box("Box1", box1dim, box1dim, box1dim);

  //
  // Box2
  //
  G4double box2dim = 8.*cm;
  auto solidBox2 = new G4Box("Box2", box2dim, box2dim, box2dim);

  //
  // Box3
  //
  G4double box3dim = 8.*cm;
  auto solidBox3 = new G4Box("Box3", box3dim, box3dim, box3dim);

  // Displacements for the shapes
  //
  G4RotationMatrix rotm = G4RotationMatrix();
  G4RotationMatrix rotm3 = G4RotationMatrix();

  rotm3.rotateY(45.*deg);

  G4ThreeVector position1 = G4ThreeVector(0.,0.,5.*cm);
  G4ThreeVector position2 = G4ThreeVector(0.,0.,15.*cm);
  G4ThreeVector position3 = G4ThreeVector(0.,0.,-15.*cm);
  
  G4Transform3D tr1 = G4Transform3D(rotm,position1);
  G4Transform3D tr2 = G4Transform3D(rotm,position2);
  G4Transform3D tr3 = G4Transform3D(rotm3,position3);

  // Initialize MultiUnion
  G4MultiUnion* munion_solid = new G4MultiUnion("Boxes_Union");

  // Add shapes
  munion_solid->AddNode(*solidBox1,tr1);
  munion_solid->AddNode(*solidBox2,tr2);
  munion_solid->AddNode(*solidBox3,tr3);

  // Close structure
  munion_solid->Voxelize();

  G4Material * uni_mat = nist->FindOrBuildMaterial("G4_Al");

  auto logicUni = new G4LogicalVolume(munion_solid, uni_mat, "Box1+Box2");
  
  G4double pos_x = -20.0*cm;
  G4double pos_y = 0.0*cm;
  G4double pos_z = 0.0*cm;

  auto physUni = new G4PVPlacement(0, G4ThreeVector(pos_x, pos_y, pos_z), logicUni, "Box1+Box2", logicWorld, false, 0, checkOverlaps);

  // Set UniBox as scoring volume
  //
  fScoringVolume = logicUni;

  //
  //always return the physical World
  //
  return physWorld;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
