//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file DetectorConstruction.cc
/// \brief Adaptation of the B1DetectorConstruction class

#include "DetectorConstruction.hh"

#include "G4RunManager.hh"
#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4LogicalVolume.hh"

#include "G4PVPlacement.hh"
#include "G4PVReplica.hh"
#include "G4PVDivision.hh"

#include "G4SystemOfUnits.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::DetectorConstruction()
: G4VUserDetectorConstruction(),
  fScoringVolume(0)
{ }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::~DetectorConstruction()
{ }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume* DetectorConstruction::Construct()
{  
  // Get nist material manager
  G4NistManager* nist = G4NistManager::Instance();
  
  // Option to switch on/off checking of volumes overlaps
  //
  G4bool checkOverlaps = true;

  //     
  // World
  //
  G4double world_sizeXY = 1.0*m;
  G4double world_sizeZ  = 1.0*m;
  G4Material* world_mat = nist->FindOrBuildMaterial("G4_AIR");
  
  auto solidWorld = new G4Box("World", 0.5*world_sizeXY, 0.5*world_sizeXY, 0.5*world_sizeZ);
  auto logicWorld = new G4LogicalVolume(solidWorld, world_mat, "World");
  auto physWorld = new G4PVPlacement(0, G4ThreeVector(), logicWorld, "World", 0, false, 0, checkOverlaps);
                     
  //     
  // Mother of a ReplicaBox
  //
  G4double RMBox_sizeX = 20.*cm;
  G4double RMBox_sizeY = 20.*cm;
  G4double RMBox_sizeZ = 1*m;
  G4Material* mbox_mat = nist->FindOrBuildMaterial("G4_Ar");
  
  auto solidRMBox = new G4Box("RMBox", 0.5*RMBox_sizeX, 0.5*RMBox_sizeY, 0.5*RMBox_sizeZ);
  auto logicRMBox = new G4LogicalVolume(solidRMBox, mbox_mat, "RMBox");
  auto physRMBox  = new G4PVPlacement(0, G4ThreeVector(-30*cm,0,0), logicRMBox, "RMBox", logicWorld, false, 0, checkOverlaps);

  //
  // Replica Daughter of a Box
  //
  G4double RDBox_sizeX = 15*cm;
  G4double RDBox_sizeY = 15*cm;
  G4double RDBox_sizeZ = 5*cm;
  G4Material* dbox_mat = nist->FindOrBuildMaterial("G4_Al");
  
  auto solidRDBox = new G4Box("RDBox", 0.5*RDBox_sizeX, 0.5*RDBox_sizeY, 0.5*RDBox_sizeZ);
  auto logicRDBox = new G4LogicalVolume(solidRDBox, dbox_mat, "RDBox");
  auto physRDBox  = new G4PVReplica("RDBox", logicRDBox, logicRMBox, kZAxis, 5, RDBox_sizeZ);
                     
  //     
  // Mother of a DivisionBox
  //
  G4double DMBox_sizeX = 20.*cm;
  G4double DMBox_sizeY = 20.*cm;
  G4double DMBox_sizeZ = 1*m;
  
  auto solidDMBox = new G4Box("DMBox", 0.5*DMBox_sizeX, 0.5*DMBox_sizeY, 0.5*DMBox_sizeZ);
  auto logicDMBox = new G4LogicalVolume(solidDMBox, mbox_mat, "DMBox");
  auto physDMBox  = new G4PVPlacement(0, G4ThreeVector(30.*cm,0,0), logicDMBox, "DMBox", logicWorld, false, 0, checkOverlaps);

  //
  // Division Daughter of a Box
  //
  G4double DDBox_sizeX = 15*cm;
  G4double DDBox_sizeY = 15*cm;
  G4double DDBox_sizeZ = 5*cm;
  
  auto solidDDBox = new G4Box("DDBox", 0.5*DDBox_sizeX, 0.5*DDBox_sizeY, 0.5*DDBox_sizeZ);
  auto logicDDBox = new G4LogicalVolume(solidDDBox, dbox_mat, "DDBox");
  auto physDDBox  = new G4PVDivision("DDBox", logicDDBox, logicDMBox, kZAxis, 5, DDBox_sizeZ, 20.*cm);
                     
  // Set Mother as scoring volume
  //
  fScoringVolume = logicRMBox;

  //
  //always return the physical World
  //
  return physWorld;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
