//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file DetectorConstruction.cc
/// \brief Adaptation of the B1DetectorConstruction class

#include "DetectorConstruction.hh"

#include "G4RunManager.hh"
#include "G4NistManager.hh"

#include "G4Box.hh"
#include "G4Sphere.hh"
#include "G4Torus.hh"

#include "G4UnionSolid.hh"
#include "G4SubtractionSolid.hh"
#include "G4IntersectionSolid.hh"

#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4SystemOfUnits.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::DetectorConstruction()
: G4VUserDetectorConstruction(),
  fScoringVolume(0)
{ }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::~DetectorConstruction()
{ }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume* DetectorConstruction::Construct()
{  
  // Get nist material manager
  G4NistManager* nist = G4NistManager::Instance();
  
  // Option to switch on/off checking of volumes overlaps
  //
  G4bool checkOverlaps = true;

  //     
  // World
  //
  G4double world_sizeXY = 1.0*m;
  G4double world_sizeZ  = 1.0*m;
  G4Material* world_mat = nist->FindOrBuildMaterial("G4_AIR");
  
  auto solidWorld = new G4Box("World", 0.5*world_sizeXY, 0.5*world_sizeXY, 0.5*world_sizeZ);
  auto logicWorld = new G4LogicalVolume(solidWorld, world_mat, "World");
  auto physWorld = new G4PVPlacement(0, G4ThreeVector(), logicWorld, "World", 0, false, 0, checkOverlaps);
                     
  //     
  // Box1
  //
  G4double box1dim = 10.*cm;
  auto solidBox1 = new G4Box("Box1", box1dim, box1dim, box1dim);

  //
  // Box2
  //
  G4double box2dim = 8.*cm;
  auto solidBox2 = new G4Box("Box2", box2dim, box2dim, box2dim);
  
  //
  // Union
  //
  auto unionBox = new G4UnionSolid("Box1+Box2", solidBox1, solidBox2, 0, G4ThreeVector(0,0,box1dim));
  G4Material * uni_mat = nist->FindOrBuildMaterial("G4_Al");

  auto logicUni = new G4LogicalVolume(unionBox, uni_mat, "Box1+Box2");
  
  G4double pos_x = -20.0*cm;
  G4double pos_y = 0.0*cm;
  G4double pos_z = 0.0*cm;

  auto physUni = new G4PVPlacement(0, G4ThreeVector(pos_x, pos_y, pos_z), logicUni, "Box1+Box2", logicWorld, false, 0, checkOverlaps);

  //
  // Subtraction
  //
  auto subtractBox = new G4SubtractionSolid("Box1-Box2", solidBox1, solidBox2, 0, G4ThreeVector(0,0,box1dim));
  G4Material * sub_mat = nist->FindOrBuildMaterial("G4_Cu");

  auto logicSub = new G4LogicalVolume(subtractBox, sub_mat, "Box1-Box2");
  
  pos_x = 20.0*cm;

  auto physSub = new G4PVPlacement(0, G4ThreeVector(pos_x, pos_y, pos_z), logicSub, "Box1-Box2", logicWorld, false, 0, checkOverlaps);


  //
  // Intersection
  //
  auto interBox = new G4IntersectionSolid("Box1&Box2", solidBox1, solidBox2, 0, G4ThreeVector(0,0,box1dim));
  G4Material * int_mat = nist->FindOrBuildMaterial("G4_Li");

  auto logicInt = new G4LogicalVolume(interBox, int_mat, "Box1&Box2");
  
  pos_x = 0.0*cm;
  pos_y = 20.0*cm;

  auto physInt = new G4PVPlacement(0, G4ThreeVector(pos_x, pos_y, pos_z), logicInt, "Box1&Box2", logicWorld, false, 0, checkOverlaps);


  G4cout << 
	  "Surface Area of union: " << unionBox->GetSurfaceArea()/cm2 << " cm2 " <<
	  " Surface Area of subtraction: " << subtractBox->GetSurfaceArea()/cm2 << " cm2 " <<
	  " Surface Area of intersection: " << interBox->GetSurfaceArea()/cm2 << " cm2 " 
	  << G4endl;

  G4cout << 
	  "Volume of union: " << unionBox->GetCubicVolume()/cm3 << " cm3 " <<
	  " Volume of subtraction: " << subtractBox->GetCubicVolume()/cm3 << " cm3 " <<
	  " Volume of intersection: " << interBox->GetCubicVolume()/cm3 << " cm3 "
	  << G4endl;

  // Set UniBox as scoring volume
  //
  fScoringVolume = logicUni;

  //
  //always return the physical World
  //
  return physWorld;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
