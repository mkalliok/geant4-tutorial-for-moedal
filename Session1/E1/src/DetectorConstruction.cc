//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file DetectorConstruction.cc
/// \brief Adaptation of the B1DetectorConstruction class

#include "DetectorConstruction.hh"

#include "G4RunManager.hh"
#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4Cons.hh"
#include "G4Orb.hh"
#include "G4Para.hh"
#include "G4Sphere.hh"
#include "G4Torus.hh"
#include "G4Trap.hh"
#include "G4Trd.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4SystemOfUnits.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::DetectorConstruction()
: G4VUserDetectorConstruction(),
  fScoringVolume(0)
{ }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::~DetectorConstruction()
{ }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume* DetectorConstruction::Construct()
{  
  // Get nist material manager
  G4NistManager* nist = G4NistManager::Instance();
  
  // Option to switch on/off checking of volumes overlaps
  //
  G4bool checkOverlaps = true;

  //     
  // World
  //
  G4double world_sizeXY = 1.0*m;
  G4double world_sizeZ  = 1.0*m;
  G4Material* world_mat = nist->FindOrBuildMaterial("G4_AIR");
  
  auto solidWorld = new G4Box("World", 0.5*world_sizeXY, 0.5*world_sizeXY, 0.5*world_sizeZ);
  auto logicWorld = new G4LogicalVolume(solidWorld, world_mat, "World");
  auto physWorld = new G4PVPlacement(0, G4ThreeVector(), logicWorld, "World", 0, false, 0, checkOverlaps);
                     
  //     
  // Cylinder
  //
  G4double innerRadius = 0.*cm;
  G4double outerRadius = 6.*cm;
  G4double hz = 25.*cm;
  G4double startAngle = 0.*deg;
  G4double endAngle = 360.*deg;

  G4Material * tub_mat = nist->FindOrBuildMaterial("G4_Al");

  auto solidTube = new G4Tubs("Cylinder", innerRadius, outerRadius, hz, startAngle, endAngle);
  auto logicTube = new G4LogicalVolume(solidTube, tub_mat, "Cylinder");
  
  G4double pos_x = -10.0*cm;
  G4double pos_y = 0.0*cm;
  G4double pos_z = 0.0*cm;

  auto physTube = new G4PVPlacement(0, G4ThreeVector(pos_x, pos_y, pos_z), logicTube, "Cylinder", logicWorld, false, 0, checkOverlaps);

  //
  // Cone
  //
  G4double minRad1 = 2.5*cm;
  G4double maxRad1 = 7.5*cm;
  G4double minRad2 = 5.*cm;
  G4double maxRad2 = 10.*cm;
  G4double lenZ = 25.*cm;
  G4double startPhi = 0.;
  G4double deltaPhi = CLHEP::pi;

  G4Material * con_mat = nist->FindOrBuildMaterial("G4_Cu");

  auto solidCone = new G4Cons("Cone", minRad1, maxRad1, minRad2, maxRad2, lenZ, startPhi, deltaPhi);
  auto logicCone = new G4LogicalVolume(solidCone, con_mat, "Cone");

  pos_x = 10.0*cm;
  
  auto physCone = new G4PVPlacement(0, G4ThreeVector(pos_x, pos_y, pos_z), logicCone, "Cone", logicWorld, false, 0, checkOverlaps);

  //
  // Orb
  //
  G4double orbRad = 5.*cm;
  G4Material * orb_mat = nist->FindOrBuildMaterial("G4_Be");

  auto solidOrb = new G4Orb("Orb", orbRad);
  auto logicOrb = new G4LogicalVolume(solidOrb, orb_mat, "Orb");
  
  pos_x = 0.;
  pos_y =10.0*cm;

  auto physOrb = new G4PVPlacement(0, G4ThreeVector(pos_x, pos_y, pos_z), logicOrb, "Orb", logicWorld, false, 0, checkOverlaps);

  //
  // Parallelpiped
  //
  G4double paraDx = 5.*cm;
  G4double paraDy = 2.5*cm;
  G4double paraDz = 10.*cm;
  G4double paraTalpha = CLHEP::pi/4.0;
  G4double paraTtheta = CLHEP::pi*1.3;
  G4double paraTphi = CLHEP::pi/2.5;

  G4Material * par_mat = nist->FindOrBuildMaterial("G4_C");

  auto solidPara = new G4Para("Para", paraDx, paraDy, paraDz, paraTalpha, paraTtheta, paraTphi);
  auto logicPara = new G4LogicalVolume(solidPara, par_mat, "Para");

  pos_y = -20.0*cm;

  auto physPara = new G4PVPlacement(0, G4ThreeVector(pos_x, pos_y, pos_z), logicPara, "Para", logicWorld, false, 0, checkOverlaps);

  // Set Cylinder as scoring volume
  //
  fScoringVolume = logicTube;

  //
  //always return the physical World
  //
  return physWorld;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
