//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file DetectorConstruction.cc
/// \brief Adaptation of the B1DetectorConstruction class

#include "DetectorConstruction.hh"

#include "G4Material.hh"
#include "G4NistManager.hh"

#include "G4RunManager.hh"
#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"

#include "G4PVPlacement.hh"
#include "G4PVReplica.hh"
#include "G4PVDivision.hh"

#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::DetectorConstruction()
: G4VUserDetectorConstruction(),
  fScoringVolume(0)
{ }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::~DetectorConstruction()
{ }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void DetectorConstruction::DefineMaterials()
{
	auto nistManager = G4NistManager::Instance();

	// Air
	nistManager->FindOrBuildMaterial("G4_AIR");
	
	// Scintillator
	nistManager->FindOrBuildMaterial("G4_PLASTIC_SC_VINYLTOLUENE");

	// CsI
	nistManager->FindOrBuildMaterial("G4_CESIUM_IODIDE");

	// Silicon
	nistManager->FindOrBuildMaterial("G4_Si");
}


G4VPhysicalVolume* DetectorConstruction::Construct()
{ 
	// Define materials
	DefineMaterials();

	auto air = G4Material::GetMaterial("G4_AIR");
	auto scintillator = G4Material::GetMaterial("G4_PLASTIC_SC_VINYTOLUENE");
	auto csi = G4Material::GetMaterial("G4_CESIUM_IODIDE");
	auto silicon = G4Material::GetMaterial("G4_Si");

  
  	// Option to switch on/off checking of volumes overlaps
  	//
  	G4bool checkOverlaps = true;

  	//     
  	// World
  	//
  	G4double world_sizeXY = 1.0*m;
  	G4double world_sizeZ  = 1.0*m;
  
  	auto solidWorld = new G4Box("World", 0.5*world_sizeXY, 0.5*world_sizeXY, 0.5*world_sizeZ);
  	auto logicWorld = new G4LogicalVolume(solidWorld, air, "World");
  	auto physWorld = new G4PVPlacement(0, G4ThreeVector(), logicWorld, "World", 0, false, 0, checkOverlaps);
        
	//
	// Envelope
	//
  	G4double Box_sizeX = 25.*cm;
  	G4double Box_sizeY = 25.*cm;
  	G4double Box_sizeZ = 50*cm;
  
  	auto solidBox = new G4Box("Envelope", 0.5*Box_sizeX, 0.5*Box_sizeY, 0.5*Box_sizeZ);
  	auto logicBox = new G4LogicalVolume(solidBox, air, "Envelope");
  	auto physBox  = new G4PVPlacement(0, G4ThreeVector(),logicBox,"Envelope", logicWorld, false, 0, checkOverlaps);


  	//     
  	// Envelope 1 
  	//
  	G4double E1Box_sizeX = 25.*cm;
  	G4double E1Box_sizeY = 5.*cm;
  	G4double E1Box_sizeZ = 50*cm;
  
  	auto solidE1Box = new G4Box("E1Box", 0.5*E1Box_sizeX, 0.5*E1Box_sizeY, 0.5*E1Box_sizeZ);
  	auto logicE1Box = new G4LogicalVolume(solidE1Box, air, "E1Box");
  	auto physE1Box  = new G4PVDivision("E1Box", logicE1Box, logicBox, kYAxis, 5, E1Box_sizeY, 0);

  	//
  	// Scintillator array
  	//
  	G4double innerRadius = 0.*cm;
  	G4double outerRadius = 2.5*cm;
  	G4double hz = 25.*cm;
	G4double startAngle = 0.*deg;
	G4double endAngle = 360.*deg;

  	auto solidScin = new G4Tubs("Scin", innerRadius, outerRadius, hz, startAngle, endAngle);
  	auto logicScin = new G4LogicalVolume(solidScin, csi, "Scin");
  	auto physScin  = new G4PVReplica("Scin", logicScin, logicE1Box, kXAxis, 5, 2*outerRadius);
                     
  	// Set Mother as scoring volume
 	//
  	fScoringVolume = logicBox;

  	//
  	//always return the physical World
  	//
  	return physWorld;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
