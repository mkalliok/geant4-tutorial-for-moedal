# Geant4 tutorial for MoEDAL

Session 1

The code is based on Geant4 basic example B1. The main modifications are done with DetectorConstruction.cc 

- E1: Basic CSG solids
- E2: Boolean solids
- E3: Multi-union structures
- E4: Replicated and Division volumes 